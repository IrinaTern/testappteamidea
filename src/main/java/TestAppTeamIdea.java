import jdo.CursValute;
import jdo.Valute;
import org.simpleframework.xml.core.Persister;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.util.List;

public class TestAppTeamIdea {

    public static void main(String[] args) throws Exception {
        CursValute cursValute;
        URL url = new URL("http://www.cbr.ru/scripts/XML_daily.asp");
        InputStream inputStream = url.openStream();
        Reader reader = new InputStreamReader(inputStream);
        Persister serializer = new Persister();

        cursValute = serializer.read(CursValute.class, reader, false);
        List<Valute> valuteList = cursValute.getValuteList();

        for (Valute valute : valuteList) {
            if (valute.getId().equals("R01200")) {
                String value = valute.getValue();
                String value1 = value.replace(',', '.');
                String nominal = valute.getNominal();

                Double valueDouble = Double.parseDouble(value1);
                Integer nominalInt = Integer.parseInt(nominal);

                System.out.println(valueDouble/nominalInt);
            }
        }
    }
}
